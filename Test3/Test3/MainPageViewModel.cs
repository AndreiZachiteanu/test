﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Test3
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        ObservableCollection<ObservableCollection<string>> _list = new ObservableCollection<ObservableCollection<string>>() { new ObservableCollection<string>() { "zet", "zet" }, new ObservableCollection<string>() { "zet", "zet" } };

        public ObservableCollection<ObservableCollection<string>> list { get { return _list; } set { _list = value; OnPropertyChanged(); } }


        ObservableCollection<string> _someList = new ObservableCollection<string> { "Zet", "Zet", "Zet", "Zet", "Zet" };

        public ObservableCollection<string> someList { get { return _someList; } set { _someList = value; OnPropertyChanged(); } }

        public MainPageViewModel()
        {



        }







        #region INotifyPropertyChanged 
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));

        }
        #endregion



    }
}
